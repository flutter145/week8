import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var risks = [
    'โรคทางเดินหายใจเรือรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรือรัง',
    'โรคหลอดเลือด',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];
  var riskValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Risk'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: riskValues[0],
              title: Text(risks[0]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[0] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[1],
              title: Text(risks[1]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[1] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[2],
              title: Text(risks[2]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[2] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[3],
              title: Text(risks[3]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[3] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[4],
              title: Text(risks[4]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[4] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[5],
              title: Text(risks[5]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[5] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: riskValues[6],
              title: Text(risks[6]),
              onChanged: (newvalue) {
                setState(() {
                  riskValues[6] = newvalue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveRisk();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values') ??
        '[false, false, false, false, false, false, false]';
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        riskValues[i] = (arrStrRiskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskValues.toString());
  }
}
